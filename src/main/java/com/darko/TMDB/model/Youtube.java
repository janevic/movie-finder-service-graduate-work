package com.darko.TMDB.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Youtube {
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("source")
	private String source;
	
	@JsonProperty("type")
	private String type;

	public Youtube() {
		super();
	}

	public Youtube(String name, String source, String type) {
		super();
		this.name = name;
		this.source = source;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
