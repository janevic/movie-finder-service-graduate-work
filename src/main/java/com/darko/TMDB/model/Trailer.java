package com.darko.TMDB.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Trailer {
	
	@JsonProperty("youtube")
	private List<Youtube> youtube;

	public Trailer() {
		super();
	}

	public Trailer(List<Youtube> youtube) {
		super();
		this.youtube = youtube;
	}

	public List<Youtube> getYoutube() {
		return youtube;
	}

	public void setYoutube(List<Youtube> youtube) {
		this.youtube = youtube;
	}
	
}
