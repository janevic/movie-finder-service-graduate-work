package com.darko.TMDB.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RootGenres {
	
	@JsonProperty("genres")
	private List<Genre> genres;

	public RootGenres() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}
	
	
}
