package com.darko.TMDB.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {
	
	@JsonProperty("backdrop_path")
	private String backdropPath;
	
	@JsonProperty("budget")
	private String budget;
	
	@JsonProperty("genres")
	private List<Genre> genres;
	
	@JsonProperty("homepage")
	private String homepage;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("imdb_id")
	private String imdbId;
	
	@JsonProperty("original_language")
	private String originalLanguage;
	
	@JsonProperty("original_title")
	private String originalTitle;
	
	@JsonProperty("overview")
	private String overview;
	
	@JsonProperty("popularity")
	private String popularity;
	
	@JsonProperty("poster_path")
	private String posterPath;
	
	/*@JsonProperty("production_companies")
	private List<Companies> productCompanies;
	
	@JsonProperty("production_countries")
	private List<Countries> productCountries;*/
	
	@JsonProperty("release_date")
	private String releaseDate;
	
	@JsonProperty("revenue")
	private String revenue;
	
	@JsonProperty("runtime")
	private String runtime;
	
	@JsonProperty("spoken_languages")
	private List<SpokenLanguages> languages;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("tagline")
	private String tagline;
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("video")
	private String video;
	
	@JsonProperty("vote_average")
	private String voteAverage;
	
	@JsonProperty("vote_count")
	private String voteCount;
	
	@JsonProperty("trailers")
	private Trailer trailer;

	public Movie() {}

	public String getBackdropPath() {
		return backdropPath;
	}

	public void setBackdropPath(String backdropPath) {
		this.backdropPath = backdropPath;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getOriginalLanguage() {
		return originalLanguage;
	}

	public void setOriginalLanguage(String originalLanguage) {
		this.originalLanguage = originalLanguage;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}

	public String getPosterPath() {
		return posterPath;
	}

	public void setPosterPath(String posterPath) {
		this.posterPath = posterPath;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTagline() {
		return tagline;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getVoteAverage() {
		return voteAverage;
	}

	public void setVoteAverage(String voteAverage) {
		this.voteAverage = voteAverage;
	}

	public String getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(String voteCount) {
		this.voteCount = voteCount;
	}

	public Trailer getTrailer() {
		return trailer;
	}

	public void setTrailer(Trailer trailer) {
		this.trailer = trailer;
	}

	@Override
	public String toString() {
		return "Movie [backdropPath=" + backdropPath 
				+ ", budget=" + budget
				+ ", genres=" + getGenres() + ", homepage=" + homepage + ", id="
				+ id + ", imdbId=" + imdbId + ", originalLanguage="
				+ originalLanguage + ", originalTitle=" + originalTitle
				+ ", overview=" + overview + ", popularity=" + popularity
				+ ", posterPath=" + posterPath + ", releaseDate=" + releaseDate
				+ ", revenue=" + revenue + ", runtime=" + runtime + ", status="
				+ status + ", tagline=" + tagline + ", title=" + title
				+ ", video=" + video + ", voteAverage=" + voteAverage
				+ ", voteCount=" + voteCount + "]";
	}
}
