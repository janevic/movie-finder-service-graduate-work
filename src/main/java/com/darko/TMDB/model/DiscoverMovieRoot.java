package com.darko.TMDB.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscoverMovieRoot {

	@JsonProperty("page")
	private int page;
	
	@JsonProperty("results")
	private List<Movie> movies;
	
	@JsonProperty("total_results")
	private int totalResults;
	
	@JsonProperty("total_pages")
	private int totalPages;

	public DiscoverMovieRoot() {
		super();
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	@Override
	public String toString() {
		return "SearchMovieRoot [page=" + page
				+ ", totalResults=" + totalResults 
				+ ", totalPages=" + totalPages + "]";
	}
	
}
