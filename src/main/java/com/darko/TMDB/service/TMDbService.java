package com.darko.TMDB.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.darko.TMDB.model.DiscoverMovieRoot;
import com.darko.TMDB.model.Movie;

@Service("TMDbService")
public class TMDbService implements ITheMovieDb {

	private static final String API_KEY_TMDb = "957dfd629fc1b1eb16c128548ae271b6";
	private Random random;
	
	@Override
	public Movie basicRandomMovie(String genres, String rating) {
		// TODO Auto-generated method stub
		
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder
				.fromUriString("http://api.themoviedb.org/3/discover/movie");
		
		if(genres != null) {
			uriComponentsBuilder.queryParam("with_genres", genres);
		}
		if(rating != null) {
			uriComponentsBuilder.queryParam("vote_average.gte", rating);
		}
		
		uriComponentsBuilder.queryParam("vote_count.gte", 50);
		uriComponentsBuilder.queryParam("api_key", API_KEY_TMDb);
		
		URI targetUrlTMDB = uriComponentsBuilder.build().toUri();
		
		DiscoverMovieRoot discoverMovieRoot = restTemplate.getForObject(targetUrlTMDB, DiscoverMovieRoot.class);
		
		int randomPage = 1;
		int randomItem = 0;
		
		random = new Random();
		
		if(discoverMovieRoot.getTotalPages() > 1) {
			randomPage = random.nextInt(discoverMovieRoot.getTotalPages());
			if(randomPage > 1) {
				//Must do another request for the movies;
				uriComponentsBuilder.queryParam("page",  randomPage);
				targetUrlTMDB = uriComponentsBuilder.build().toUri();
				
				discoverMovieRoot = restTemplate.getForObject(targetUrlTMDB, DiscoverMovieRoot.class);
				
				randomItem = random.nextInt(discoverMovieRoot.getMovies().size());
			}
		}
		
		if(randomPage == 1) {
			randomItem = random.nextInt(discoverMovieRoot.getMovies().size());
		}
		
		Movie movie = discoverMovieRoot.getMovies().get(randomItem);
		
		uriComponentsBuilder = UriComponentsBuilder
				.fromUriString("http://api.themoviedb.org/3/movie/"+movie.getId())
				.queryParam("api_key", API_KEY_TMDb)
				.queryParam("append_to_response", "releases,trailers");
		
		targetUrlTMDB = uriComponentsBuilder.build().toUri();
		movie = restTemplate.getForObject(targetUrlTMDB, Movie.class);
		
		return movie;
	}

	@Override
	public List<Movie> listRandomMovies(String genres, String rating) {
		// TODO Auto-generated method stub
		
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder
				.fromUriString("http://api.themoviedb.org/3/discover/movie");
		
		if(genres != null) {
			uriComponentsBuilder.queryParam("with_genres", genres);
		}
		if(rating != null) {
			uriComponentsBuilder.queryParam("vote_average.gte", rating);
		}
		
		uriComponentsBuilder.queryParam("vote_count.gte", 30);
		uriComponentsBuilder.queryParam("api_key", API_KEY_TMDb);
		uriComponentsBuilder.queryParam("append_to_response", "releases,trailers");
		
		URI targetUrlTMDB = uriComponentsBuilder.build().toUri();
		
		DiscoverMovieRoot discoverMovieRoot = restTemplate.getForObject(targetUrlTMDB, DiscoverMovieRoot.class);
		
		int randomPage = 1;
		
		random = new Random();
		if(discoverMovieRoot != null) {
			if(discoverMovieRoot.getTotalPages() > 1) {
				randomPage = random.nextInt(discoverMovieRoot.getTotalPages());
				if(randomPage > 1) {
					//Must do another request for the movies;
					uriComponentsBuilder.queryParam("page",  randomPage);
					targetUrlTMDB = uriComponentsBuilder.build().toUri();
					discoverMovieRoot = restTemplate.getForObject(targetUrlTMDB, DiscoverMovieRoot.class);
				}
			}
			List<Movie> movies = new ArrayList<Movie>(discoverMovieRoot.getMovies());
			if(movies.size() > 0) {
				return movies;
			}
		}
		return null;
	}
	
	@Override
	public Movie getDetailsForMovie(Movie movie) {
		
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder
				.fromUriString("http://api.themoviedb.org/3/movie/"+movie.getId())
				.queryParam("api_key", API_KEY_TMDb)
				.queryParam("append_to_response", "releases,trailers");
		
		URI targetUrlTMDB = uriComponentsBuilder.build().toUri();
		movie = restTemplate.getForObject(targetUrlTMDB, Movie.class);
		
		return movie;
	}

	public List<Movie> getMovieByTitle(String title) {
		
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder
				.fromUriString("http://api.themoviedb.org/3/search/movie");
		uriComponentsBuilder.queryParam("query", title);
		uriComponentsBuilder.queryParam("api_key", API_KEY_TMDb);
		
		URI targetUrlTMDB = uriComponentsBuilder.build().toUri();
		
		List<Movie> listMovies = restTemplate.getForObject(targetUrlTMDB, DiscoverMovieRoot.class).getMovies();
		
		return listMovies;
	}
	
}
