package com.darko.TMDB.service;

import java.util.List;

import com.darko.TMDB.model.Movie;

public interface ITheMovieDb {

	Movie basicRandomMovie(String genres, String rating);
	
	/*
	 * Lists random movies from The Movie Db;
	 * Case 1: it receives genres and rating as parameter and returns list of movies based on them. 
	 * 			If no genre or rating is presented they are ignored and the suggested movies are not limited;
	 * Case 2: If the service TMDB does not return movies for the presented parameters then null should be returned;
	 * Case 3: If the service TMDB is not available null should be returned;
	 * */
	List<Movie> listRandomMovies(String genres, String rating);
	
	/*
	 * Searches the movie and gets the needed data;
	 * Case 1: recives Movie and returns movie with additional data from The Movie Db
	 * */
	Movie getDetailsForMovie(Movie movie);
}
