package com.darko.main.service;

import java.security.SecureRandom;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("EncoderService")
public class EncoderService {
	
	private BCryptPasswordEncoder bcryptEncoder;
	
	private SecureRandom secureRandom;
	
	public EncoderService() {
		secureRandom = new SecureRandom();
	}
	
	public BCryptPasswordEncoder getBCryptEncoder() {
		bcryptEncoder = new BCryptPasswordEncoder();
		return bcryptEncoder;
	}
	
	public int random() {
		return secureRandom.nextInt(999999);
	}
}
