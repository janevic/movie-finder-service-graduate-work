package com.darko.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.darko.main.db.model.Account;
import com.darko.main.db.model.AccountUser;
import com.darko.main.db.repo.UserRepository;
import com.google.common.collect.Sets;

@Component
public class AccountUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository accountRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		Account a = accountRepository.findByUsername(username);
		if (a != null) {
			return new AccountUser(a.getUsername(), a.getPassword(),
					Sets.newHashSet(new SimpleGrantedAuthority("ROLE_USER")));
		} else {
			return null;
		}
	}

}
