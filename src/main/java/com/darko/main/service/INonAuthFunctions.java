package com.darko.main.service;

import java.util.List;

import com.darko.main.model.SuggestedMovie;

public interface INonAuthFunctions {

	SuggestedMovie movieSuggestion(String genres, String rating );
	
	List<SuggestedMovie> searchMovieByTitle(String title);
	
	SuggestedMovie fullMovieDetails(String tmdb_id);
}
