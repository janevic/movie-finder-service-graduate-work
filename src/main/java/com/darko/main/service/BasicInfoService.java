package com.darko.main.service;

import java.net.URI;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.darko.TMDB.model.RootGenres;
import com.darko.main.db.repo.RepositoryConfiguration;

@SpringApplicationConfiguration(classes = {RepositoryConfiguration.class})
@Service("BasicsInfoService")
public class BasicInfoService implements IBasicsInfo {

	private static final String API_KEY_TMDb = "957dfd629fc1b1eb16c128548ae271b6";
	
	@Override
	public RootGenres genres() {
		
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder
				.fromUriString("http://api.themoviedb.org/3/genre/movie/list")
				.queryParam("api_key", API_KEY_TMDb);
		
		URI targetUrlTMDB = uriComponentsBuilder.build().toUri();
		
		RootGenres rootGenres = restTemplate.getForObject(targetUrlTMDB, RootGenres.class);
		
		return rootGenres;
	}

	@Override
	public String usageData() {
		// TODO Auto-generated method stub
		return null;
	}

}
