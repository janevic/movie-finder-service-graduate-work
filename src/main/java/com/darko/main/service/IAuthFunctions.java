package com.darko.main.service;

import java.util.List;

import com.darko.main.model.RootData;
import com.darko.main.model.UserInfoStats;
import com.darko.main.model.UserPreferencesResponse;

public interface IAuthFunctions {

	RootData authMovieSuggestion(String genres, String rating, Long userId, String username);
	
	List<String> listOfWatchedMovies(Long userId);
	
	UserInfoStats addWatchedMovie(String movieName, String imdbId, Long userId, String username);
	
	UserInfoStats saveUserPreferences(float rating, String genres, Long userId, String username);
	
	UserPreferencesResponse loadUserPreferences(Long userId, String username);
}
