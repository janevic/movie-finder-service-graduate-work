package com.darko.main.service;

import com.darko.TMDB.model.RootGenres;

public interface IBasicsInfo {

	RootGenres genres();
	
	String usageData();
}
