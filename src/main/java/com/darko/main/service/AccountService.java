package com.darko.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.darko.main.db.model.Account;
import com.darko.main.db.repo.UserRepository;
import com.darko.main.model.UserInfoStats;

@Service("AccountService")
public class AccountService {

	@Autowired
	@Qualifier("EncoderService")
	private EncoderService bcryptPasswordEncoder;

	@Autowired
	private UserRepository accountRepository;
	
	public Account createAccount(Account acc) {
		String pass = bcryptPasswordEncoder.getBCryptEncoder().encode(acc.getPassword());
		Account checkAcc = accountRepository.findByUsername(acc.getUsername());
		if(checkAcc == null) {
			Account account = new Account(acc.getUsername(), pass);
			return accountRepository.save(account);
		}
		else {
			checkAcc.setMsg("The user already exists");
			return checkAcc;
		}
	}
	
	public UserInfoStats checkUserValidity(Account acc) {
		UserInfoStats infoStats = new UserInfoStats();
		Account a = accountRepository.findByUsername(acc.getUsername());
		
		if(a != null) {
			infoStats.setStatus("success");
			infoStats.setUser_id(a.getId());
			infoStats.setUsername(a.getUsername());
			infoStats.setMessage("User exists");
		}
		else {
			infoStats.setStatus("fail");
			infoStats.setUser_id(null);
			infoStats.setUsername(null);
			infoStats.setMessage("User does not exists");
		}
		return infoStats;
	}
}
