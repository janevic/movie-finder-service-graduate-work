package com.darko.main.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.stereotype.Service;

import com.darko.OMDB.model.OMDbMovieData;
import com.darko.OMDB.service.OMDbService;
import com.darko.TMDB.model.Genre;
import com.darko.TMDB.model.Movie;
import com.darko.TMDB.model.Youtube;
import com.darko.TMDB.service.TMDbService;
import com.darko.YouTube.service.WatchMovieService;
import com.darko.main.db.repo.RepositoryConfiguration;
import com.darko.main.model.SuggestedMovie;

@SpringApplicationConfiguration(classes = { RepositoryConfiguration.class })
@Service("NonAuthService")
public class NonAuthService implements INonAuthFunctions {

	@Autowired
	@Qualifier("TMDbService")
	private TMDbService tmdbService;

	@Autowired
	@Qualifier("OMDbService")
	private OMDbService omdbService;

	@Autowired
	@Qualifier("WatchMoviesService")
	private WatchMovieService youtubeService;

	@Autowired
	@Qualifier("BasicsInfoService")
	private BasicInfoService basicInfoService;

	private Random random = new Random();

	@Override
	public SuggestedMovie movieSuggestion(String genres, String rating) {
		// This function get's a random movie from TheMovieDb
		// then it get's it's IMDb ID and searches the OMDb
		// service for the same movie in order to enrich
		// the data for the movie;

		List<Movie> movies = tmdbService.listRandomMovies(genres, rating);
		int randomItem = random.nextInt(movies.size());

		// Fetch the details for the chosen movie
		Movie tmdbMovie = tmdbService.getDetailsForMovie(movies.get(randomItem));

		SuggestedMovie suggestedMovie = null;

		OMDbMovieData omdbMovie = null;
		
		if (tmdbMovie.getImdbId() != null) {
			omdbMovie = omdbService.searchMoviesByIMDbID(tmdbMovie.getImdbId());
		} else if (tmdbMovie.getTitle() != null) {
			omdbMovie = omdbService.searchMoviesByTitle(tmdbMovie.getTitle());
		}
		if(omdbMovie != null) {
			fixOMDBMovie(omdbMovie);
		}
		suggestedMovie = new SuggestedMovie();

		if (tmdbMovie != null) {
			suggestedMovie.setImdbId(tmdbMovie.getImdbId());
			suggestedMovie.setTheMovieDbId(tmdbMovie.getId());
			suggestedMovie.setTitle(tmdbMovie.getTitle());
			suggestedMovie.setDescription(tmdbMovie.getOverview());
			suggestedMovie.setPosterURL(tmdbMovie.getPosterPath());
			suggestedMovie.setGenres(tmdbMovie.getGenres());
			suggestedMovie.setBudget(tmdbMovie.getBudget());
			suggestedMovie.setHomepage(tmdbMovie.getHomepage());
			suggestedMovie.setPopularity(tmdbMovie.getPopularity());
			suggestedMovie.setReleaseDate(tmdbMovie.getReleaseDate());
			suggestedMovie.setRevenue(tmdbMovie.getRevenue());
			suggestedMovie.setRuntime(tmdbMovie.getRuntime());
			suggestedMovie.setVoteAverageTMDB(tmdbMovie.getVoteAverage());
			suggestedMovie.setVoteCountTMDB(tmdbMovie.getVoteCount());

			try {
				List<String> trailers = null;
				if (!tmdbMovie.getTrailer().getYoutube().isEmpty()) {
					trailers = new ArrayList<String>();
					List<Youtube> videos = tmdbMovie.getTrailer().getYoutube();
					for (Youtube trailer : videos) {
						if (trailer.getType().equals("Trailer")) {
							String t = "https://www.youtube.com/embed/" + trailer.getSource();
							trailers.add(t);
						}
					}
				}
				suggestedMovie.setYouTubeURLTrailers(trailers);
			} catch (IndexOutOfBoundsException error) {
				error.printStackTrace();
			}

			suggestedMovie.setYouTubeURLMovie(youtubeService.findMovieOnYoutube(tmdbMovie.getTitle()));
		}

		if (omdbMovie != null) {
			if (suggestedMovie.getTitle() == null) {
				suggestedMovie.setTitle(omdbMovie.getTitle());
			}
			suggestedMovie.setHomepage(omdbMovie.getWebsite());
			suggestedMovie.setLanguage(omdbMovie.getLanguage());

			List<String> directors = new ArrayList<String>();
			if(omdbMovie.getDirector() != null) {
				for (String director : omdbMovie.getDirector().split(",")) {
					directors.add(director);
				}
			}
			suggestedMovie.setDirector(directors);

			List<String> writers = new ArrayList<String>();
			if(omdbMovie.getWriter() != null) {
				for (String writer : omdbMovie.getWriter().split(",")) {
					writers.add(writer);
				}
			}
			suggestedMovie.setWriter(writers);

			List<String> actors = new ArrayList<String>();
			if(omdbMovie.getActors() != null) {
				for (String actor : omdbMovie.getActors().split(",")) {
					actors.add(actor);
				}
			}
			suggestedMovie.setActors(actors);

			suggestedMovie.setAwards(omdbMovie.getAwards());
			suggestedMovie.setCountry(omdbMovie.getCountry());
			suggestedMovie.setMetascore(omdbMovie.getMetascore());
			suggestedMovie.setImdbRating(omdbMovie.getImdbRating());
			suggestedMovie.setImdbVotes(omdbMovie.getImdbVotes());
			suggestedMovie.setTomatoMeter(omdbMovie.getTomatoMeter());
			suggestedMovie.setTomatoRating(omdbMovie.getTomatoRating());
			suggestedMovie.setTomatoReviews(omdbMovie.getTomatoReviews());
			suggestedMovie.setTomatoFresh(omdbMovie.getTomatoFresh());
			suggestedMovie.setTomatoRotten(omdbMovie.getTomatoRotten());
			suggestedMovie.setTomatoConsensus(omdbMovie.getTomatoConsensus());
			suggestedMovie.setTomatoUserMeter(omdbMovie.getTomatoUserMeter());
			suggestedMovie.setTomatoUserRating(omdbMovie.getTomatoUserRating());
			suggestedMovie.setTomatoUserReviews(omdbMovie.getTomatoUserReviews());
			suggestedMovie.setTomatoURL(omdbMovie.getTomatoURL());
		}

		return suggestedMovie;
	}

	public List<SuggestedMovie> searchMovieByTitle(String title) {
		List<SuggestedMovie> listSuggestedMovies = new ArrayList<SuggestedMovie>();

		/*
		 * Searching in the omdbapi
		 */
		OMDbMovieData movieOMDB = omdbService.searchMoviesByTitle(title);
		if(movieOMDB != null) {
			fixOMDBMovie(movieOMDB);
		}
		/*
		 * Searching in the themovieDb
		 */
		List<Movie> listMoviesTMDB = tmdbService.getMovieByTitle(title);

		/*
		 * Matching the data from the OMDB api and TMDB api
		 */
		if(movieOMDB != null) {
			for (Movie m : listMoviesTMDB) {
	
				SuggestedMovie movieToBeAdded = new SuggestedMovie();
	
				// The movie that has highest probability to be the one that the
				// user is searching for
				if (movieOMDB.getTitle().trim().equals(m.getTitle().trim())) {
					movieToBeAdded.setTitle(movieOMDB.getTitle());
	
					List<String> directors = new ArrayList<String>();
					if(movieOMDB.getDirector() != null) {
						for (String director : movieOMDB.getDirector().split(",")) {
							directors.add(director);
						}
					}
					movieToBeAdded.setDirector(directors);
	
					List<String> writers = new ArrayList<String>();
					if(movieOMDB.getWriter() != null) {
						for (String writer : movieOMDB.getWriter().split(",")) {
							writers.add(writer);
						}
					}
					movieToBeAdded.setWriter(writers);
	
					List<String> actors = new ArrayList<String>();
					if(movieOMDB.getActors() != null) {
						for (String actor : movieOMDB.getActors().split(",")) {
							actors.add(actor);
						}
					}
					movieToBeAdded.setActors(actors);
	
					movieToBeAdded.setAwards(movieOMDB.getAwards());
	
					movieToBeAdded.setCountry(movieOMDB.getCountry());
	
					String[] genres = movieOMDB.getGenre().split(" ");
					List<Genre> lstGenre = new ArrayList<Genre>();
					for (String g : genres) {
						lstGenre.add(new Genre(g));
					}
					movieToBeAdded.setGenres(lstGenre);
					movieToBeAdded.setImdbId(movieOMDB.getImdbId());
					movieToBeAdded.setImdbRating(movieOMDB.getImdbRating());
					movieToBeAdded.setImdbVotes(movieOMDB.getImdbVotes());
					movieToBeAdded.setLanguage(movieOMDB.getLanguage());
					movieToBeAdded.setMetascore(movieOMDB.getMetascore());
					movieToBeAdded.setReleaseDate(movieOMDB.getReleased());
					movieToBeAdded.setRuntime(movieOMDB.getRuntime());
					movieToBeAdded.setTomatoConsensus(movieOMDB.getTomatoConsensus());
					movieToBeAdded.setTomatoFresh(movieOMDB.getTomatoFresh());
					movieToBeAdded.setTomatoMeter(movieOMDB.getTomatoMeter());
					movieToBeAdded.setTomatoRating(movieOMDB.getTomatoRating());
					movieToBeAdded.setTomatoReviews(movieOMDB.getTomatoReviews());
					movieToBeAdded.setTomatoRotten(movieOMDB.getTomatoRotten());
					movieToBeAdded.setTomatoURL(movieOMDB.getTomatoURL());
					movieToBeAdded.setTomatoUserMeter(movieOMDB.getTomatoUserMeter());
					movieToBeAdded.setTomatoUserRating(movieOMDB.getTomatoUserRating());
					movieToBeAdded.setTomatoUserReviews(movieOMDB.getTomatoUserReviews());
				}
				if (movieToBeAdded.getTitle() == null) {
					movieToBeAdded.setTitle(m.getTitle());
				}
				movieToBeAdded.setBudget(m.getBudget());
				movieToBeAdded.setDescription(m.getOverview());
				// movieToBeAdded.setHomepage(homepage); - no homepage on these
				// requests
				if (movieToBeAdded.getLanguage() == null) {
					movieToBeAdded.setLanguage(m.getOriginalLanguage());
				}
				movieToBeAdded.setPopularity(m.getPopularity());
				movieToBeAdded.setPosterURL(m.getPosterPath());
				if (movieToBeAdded.getGenres() == null) {
					movieToBeAdded.setGenres(m.getGenres());
				}
				if (movieToBeAdded.getReleaseDate() == null) {
					movieToBeAdded.setReleaseDate(m.getReleaseDate());
				}
				// movieToBeAdded.setRevenue(m.getRevenue()); - no revenue on
				// there requests
				// movieToBeAdded.setRuntime(m.getRuntime()); - no runtime in
				// TMDB
				movieToBeAdded.setTheMovieDbId(m.getId());
				movieToBeAdded.setVoteAverageTMDB(m.getVoteAverage());
				movieToBeAdded.setVoteCountTMDB(m.getVoteCount());
				// movieToBeAdded.setYouTubeURLMovie(youTubeURLMovie); - no
				// youtube trailer on these requests
				// movieToBeAdded.setYouTubeURLTrailers(youTubeURLTrailers); -
				// no trailers on these requests
	
				listSuggestedMovies.add(movieToBeAdded);
			}
		}
		else {
			for (Movie m : listMoviesTMDB) {
				SuggestedMovie movieToBeAdded = new SuggestedMovie();
				movieToBeAdded.setTheMovieDbId(m.getId());
				movieToBeAdded.setImdbId(m.getImdbId());
				movieToBeAdded.setPosterURL(m.getPosterPath());
				movieToBeAdded.setDescription(m.getOverview());
				listSuggestedMovies.add(movieToBeAdded);
			}
		}

		return listSuggestedMovies;
	}

	public SuggestedMovie fullMovieDetails(String tmdb_id) {
		SuggestedMovie movieToBeAdded = new SuggestedMovie();

		Movie movie = new Movie();
		movie.setId(tmdb_id);
		/*
		 * Searching in the themovieDb
		 */
		Movie movieTMDB = tmdbService.getDetailsForMovie(movie);
		
		/*
		 * Searching in the omdbapi
		 */
		OMDbMovieData movieOMDB = omdbService.searchMoviesByTitle(movieTMDB.getTitle());
		if(movieOMDB != null) {
			fixOMDBMovie(movieOMDB);
		}
		
		if (movieToBeAdded.getTitle() == null) {
			movieToBeAdded.setTitle(movieTMDB.getTitle());
		}
		movieToBeAdded.setBudget(movieTMDB.getBudget());
		movieToBeAdded.setDescription(movieTMDB.getOverview());
		movieToBeAdded.setHomepage(movieTMDB.getHomepage());
		if (movieToBeAdded.getLanguage() == null) {
			movieToBeAdded.setLanguage(movieTMDB.getOriginalLanguage());
		}
		movieToBeAdded.setPopularity(movieTMDB.getPopularity());
		movieToBeAdded.setPosterURL(movieTMDB.getPosterPath());
		if (movieToBeAdded.getGenres() == null) {
			movieToBeAdded.setGenres(movieTMDB.getGenres());
		}
		if (movieToBeAdded.getReleaseDate() == null) {
			movieToBeAdded.setReleaseDate(movieTMDB.getReleaseDate());
		}
		movieToBeAdded.setRevenue(movieTMDB.getRevenue());
		movieToBeAdded.setRuntime(movieTMDB.getRuntime());
		movieToBeAdded.setTheMovieDbId(movieTMDB.getId());
		movieToBeAdded.setVoteAverageTMDB(movieTMDB.getVoteAverage());
		movieToBeAdded.setVoteCountTMDB(movieTMDB.getVoteCount());
		
		if(movieOMDB != null) {
		if((movieOMDB.getTitle() != null) && (movieTMDB.getTitle() != null)) {
			if (movieOMDB.getTitle().trim().equals(movieTMDB.getTitle().trim())) {
				movieToBeAdded.setTitle(movieOMDB.getTitle());
	
				List<String> directors = new ArrayList<String>();
				if(movieOMDB.getDirector() != null) {
					for (String director : movieOMDB.getDirector().split(",")) {
						directors.add(director);
					}
				}
				movieToBeAdded.setDirector(directors);
	
				List<String> writers = new ArrayList<String>();
				if(movieOMDB.getWriter() != null) {
					for (String writer : movieOMDB.getWriter().split(",")) {
						writers.add(writer);
					}
				}
				movieToBeAdded.setWriter(writers);
	
				List<String> actors = new ArrayList<String>();
				if(movieOMDB.getActors() != null) {
					for (String actor : movieOMDB.getActors().split(",")) {
						actors.add(actor);
					}
				}
				movieToBeAdded.setActors(actors);
	
				movieToBeAdded.setAwards(movieOMDB.getAwards());
				movieToBeAdded.setCountry(movieOMDB.getCountry());
				if (movieToBeAdded.getGenres().isEmpty()) {
					// https://api.themoviedb.org/3/genre/movie/list?api_key=957dfd629fc1b1eb16c128548ae271b6
					List<Genre> availableGenres = basicInfoService.genres().getGenres();
					String[] genres = movieOMDB.getGenre().split(",");
					for (int i = 0; i < genres.length; i++) {
						genres[i] = genres[i].trim();
					}
					List<Genre> lstGenre = new ArrayList<Genre>();
					for (Genre g : availableGenres) {
						for (String s : genres) {
							if (g.getName().equals(s)) {
								lstGenre.add(g);
							}
						}
					}
					movieToBeAdded.setGenres(lstGenre);
				}
				movieToBeAdded.setImdbId(movieOMDB.getImdbId());
				movieToBeAdded.setImdbRating(movieOMDB.getImdbRating());
				movieToBeAdded.setImdbVotes(movieOMDB.getImdbVotes());
				movieToBeAdded.setLanguage(movieOMDB.getLanguage());
				movieToBeAdded.setMetascore(movieOMDB.getMetascore());
				movieToBeAdded.setReleaseDate(movieOMDB.getReleased());
				movieToBeAdded.setRuntime(movieOMDB.getRuntime());
				movieToBeAdded.setTomatoConsensus(movieOMDB.getTomatoConsensus());
				movieToBeAdded.setTomatoFresh(movieOMDB.getTomatoFresh());
				movieToBeAdded.setTomatoMeter(movieOMDB.getTomatoMeter());
				movieToBeAdded.setTomatoRating(movieOMDB.getTomatoRating());
				movieToBeAdded.setTomatoReviews(movieOMDB.getTomatoReviews());
				movieToBeAdded.setTomatoRotten(movieOMDB.getTomatoRotten());
				movieToBeAdded.setTomatoURL(movieOMDB.getTomatoURL());
				movieToBeAdded.setTomatoUserMeter(movieOMDB.getTomatoUserMeter());
				movieToBeAdded.setTomatoUserRating(movieOMDB.getTomatoUserRating());
				movieToBeAdded.setTomatoUserReviews(movieOMDB.getTomatoUserReviews());
			}
		}
		}

		try {
			List<String> trailers = null;
			if (!movieTMDB.getTrailer().getYoutube().isEmpty()) {
				trailers = new ArrayList<String>();
				List<Youtube> videos = movieTMDB.getTrailer().getYoutube();
				for (Youtube trailer : videos) {
					if (trailer.getType().equals("Trailer")) {
						String t = "https://www.youtube.com/embed/" + trailer.getSource();
						trailers.add(t);
					}
				}
			}
			movieToBeAdded.setYouTubeURLTrailers(trailers);
		} catch (IndexOutOfBoundsException error) {
			error.printStackTrace();
		}

		movieToBeAdded.setYouTubeURLMovie(youtubeService.findMovieOnYoutube(movieTMDB.getTitle()));

		return movieToBeAdded;
	}

	private void fixOMDBMovie(OMDbMovieData movie) {
		if(movie.getTitle() != null) {
			if (movie.getActors().equals("N/A")) {
				movie.setActors(null);
			}
			if (movie.getAwards().equals("N/A")) {
				movie.setAwards(null);
			}
			if (movie.getBoxOffice().equals("N/A")) {
				movie.setBoxOffice(null);
			}
			if (movie.getCountry().equals("N/A")) {
				movie.setCountry(null);
			}
			if (movie.getDirector().equals("N/A")) {
				movie.setDirector(null);
			}
			if (movie.getDvd().equals("N/A")) {
				movie.setDvd(null);
			}
			if (movie.getGenre().equals("N/A")) {
				movie.setGenre(null);
			}
			if (movie.getImdbId().equals("N/A")) {
				movie.setImdbId(null);
			}
			if (movie.getImdbRating().equals("N/A")) {
				movie.setImdbRating(null);
			}
			if (movie.getImdbVotes().equals("N/A")) {
				movie.setImdbVotes(null);
			}
			if (movie.getLanguage().equals("N/A")) {
				movie.setLanguage(null);
			}
			if (movie.getMetascore().equals("N/A")) {
				movie.setMetascore(null);
			}
			if (movie.getPlot().equals("N/A")) {
				movie.setPlot(null);
			}
			if (movie.getPoster().equals("N/A")) {
				movie.setPoster(null);
			}
			if (movie.getProduction().equals("N/A")) {
				movie.setProduction(null);
			}
			if (movie.getRated().equals("N/A")) {
				movie.setRated(null);
			}
			if (movie.getReleased().equals("N/A")) {
				movie.setReleased(null);
			}
			if (movie.getRuntime().equals("N/A")) {
				movie.setRuntime(null);
			}
			if (movie.getTitle().equals("N/A")) {
				movie.setTitle(null);
			}
			if (movie.getTomatoConsensus().equals("N/A")) {
				movie.setTomatoConsensus(null);
			}
			if (movie.getTomatoFresh().equals("N/A")) {
				movie.setTomatoFresh(null);
			}
			if (movie.getTomatoMeter().equals("N/A")) {
				movie.setTomatoMeter(null);
			}
			if (movie.getTomatoRating().equals("N/A")) {
				movie.setTomatoRating(null);
			}
			if (movie.getTomatoReviews().equals("N/A")) {
				movie.setTomatoReviews(null);
			}
			if (movie.getTomatoRotten().equals("N/A")) {
				movie.setTomatoRotten(null);
			}
			if (movie.getTomatoURL().equals("N/A")) {
				movie.setTomatoURL(null);
			}
			if (movie.getTomatoUserMeter().equals("N/A")) {
				movie.setTomatoUserMeter(null);
			}
			if (movie.getTomatoUserRating().equals("N/A")) {
				movie.setTomatoUserRating(null);
			}
			if (movie.getTomatoUserReviews().equals("N/A")) {
				movie.setTomatoUserReviews(null);
			}
			if (movie.getType().equals("N/A")) {
				movie.setType(null);
			}
			if (movie.getWebsite().equals("N/A")) {
				movie.setWebsite(null);
			}
			if (movie.getWriter().equals("N/A")) {
				movie.setWriter(null);
			}
			if (movie.getYear().equals("N/A")) {
				movie.setYear(null);
			}
		}
	}

}
