package com.darko.main.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.stereotype.Service;

import com.darko.OMDB.model.OMDbMovieData;
import com.darko.OMDB.service.OMDbService;
import com.darko.TMDB.model.Movie;
import com.darko.TMDB.model.Youtube;
import com.darko.TMDB.service.TMDbService;
import com.darko.YouTube.service.WatchMovieService;
import com.darko.main.db.model.UserPreferences;
import com.darko.main.db.model.Account;
import com.darko.main.db.model.WatchedMovie;
import com.darko.main.db.repo.RepositoryConfiguration;
import com.darko.main.db.repo.UserPreferencesRepository;
import com.darko.main.db.repo.UserRepository;
import com.darko.main.db.repo.WatchedMovieRepository;
import com.darko.main.model.RootData;
import com.darko.main.model.SuggestedMovie;
import com.darko.main.model.UserInfoStats;
import com.darko.main.model.UserPreferencesResponse;

@SpringApplicationConfiguration(classes = { RepositoryConfiguration.class })
@Service("AuthService")
public class AuthService implements IAuthFunctions {

	@Autowired
	@Qualifier("TMDbService")
	private TMDbService tmdbService;

	@Autowired
	@Qualifier("OMDbService")
	private OMDbService omdbService;

	@Autowired
	@Qualifier("WatchMoviesService")
	private WatchMovieService youtubeService;

	private Random random = new Random();

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	private WatchedMovieRepository watchedMovieRepository;

	@Autowired
	private UserPreferencesRepository userPreferencesRepository;

	@Autowired
	private UserRepository userRepository;

	public RootData authMovieSuggestion(String genres, String rating,
			Long userId, String username) {
		// TODO Auto-generated method stub

		Account user = userRepository.findById(userId);

		RootData rootData = new RootData();
		UserInfoStats userInfoStats = new UserInfoStats();
		SuggestedMovie suggestedMovie = null;

		if (user != null) {
			
			// GET USER DATA

			userInfoStats.setStatus("success");
			userInfoStats.setUsername(user.getUsername());
			userInfoStats.setUser_id(user.getId());

			List<WatchedMovie> watchedMoviesDb = watchedMovieRepository.findAllByUserId(user.getId());
			List<Movie> movies = clearListFromWatchedMovies(watchedMoviesDb, genres, rating, 0);
			if(movies != null) {
			if(movies.size() != 0) {
				int randomItem = random.nextInt(movies.size());
	
				suggestedMovie = new SuggestedMovie();

				// Fetch the details for the chosen movie
				Movie tmdbMovie = tmdbService.getDetailsForMovie(movies.get(randomItem));

				OMDbMovieData omdbMovie = null;
				if (tmdbMovie.getImdbId() != null) {
					omdbMovie = omdbService.searchMoviesByIMDbID(tmdbMovie.getImdbId());
				} else if (tmdbMovie.getTitle() != null) {
					omdbMovie = omdbService.searchMoviesByTitle(tmdbMovie.getTitle());
				}
				if(omdbMovie != null) {
					fixOMDBMovie(omdbMovie);
				}
			
				if (tmdbMovie != null) {
					suggestedMovie.setImdbId(tmdbMovie.getImdbId());
					suggestedMovie.setTheMovieDbId(tmdbMovie.getId());
					suggestedMovie.setTitle(tmdbMovie.getTitle());
					suggestedMovie.setDescription(tmdbMovie.getOverview());
					suggestedMovie.setPosterURL(tmdbMovie.getPosterPath());
					suggestedMovie.setGenres(tmdbMovie.getGenres());
					suggestedMovie.setBudget(tmdbMovie.getBudget());
					suggestedMovie.setHomepage(tmdbMovie.getHomepage());
					suggestedMovie.setPopularity(tmdbMovie.getPopularity());
					suggestedMovie.setReleaseDate(tmdbMovie.getReleaseDate());
					suggestedMovie.setRevenue(tmdbMovie.getRevenue());
					suggestedMovie.setRuntime(tmdbMovie.getRuntime());
					suggestedMovie.setVoteAverageTMDB(tmdbMovie.getVoteAverage());
					suggestedMovie.setVoteCountTMDB(tmdbMovie.getVoteCount());
	
					try {
						List<String> trailers = null;
						if (!tmdbMovie.getTrailer().getYoutube().isEmpty()) {
							trailers = new ArrayList<String>();
							List<Youtube> videos = tmdbMovie.getTrailer().getYoutube();
							for(Youtube trailer : videos) {
								if(trailer.getType().equals("Trailer")) {
									String t = "https://www.youtube.com/embed/" + trailer.getSource();
									trailers.add(t);
								}
							}
						}
						suggestedMovie.setYouTubeURLTrailers(trailers);
					} catch (IndexOutOfBoundsException error) {
						error.printStackTrace();
					}
	
					suggestedMovie.setYouTubeURLMovie(youtubeService
							.findMovieOnYoutube(tmdbMovie.getTitle()));
				}

				if (omdbMovie != null) {
					if (suggestedMovie.getTitle() == null) {
						suggestedMovie.setTitle(omdbMovie.getTitle());
					}
					suggestedMovie.setHomepage(omdbMovie.getWebsite());
					suggestedMovie.setLanguage(omdbMovie.getLanguage());
					
					List<String> directors = new ArrayList<String>();
					if(omdbMovie.getDirector() != null) {
						for(String director : omdbMovie.getDirector().split(",")) {
							directors.add(director);
						}
					}
					suggestedMovie.setDirector(directors);
					
					List<String> writers = new ArrayList<String>();
					if(omdbMovie.getWriter() != null) {
						for(String writer : omdbMovie.getWriter().split(",")) {
							writers.add(writer);
						}
					}
					suggestedMovie.setWriter(writers);
					
					List<String> actors = new ArrayList<String>();
					if(omdbMovie.getActors() != null) {
						for(String actor : omdbMovie.getActors().split(",")) {
							actors.add(actor);
						}
					}
					suggestedMovie.setActors(actors);
	
					suggestedMovie.setAwards(omdbMovie.getAwards());				
					suggestedMovie.setCountry(omdbMovie.getCountry());
					suggestedMovie.setMetascore(omdbMovie.getMetascore());
					suggestedMovie.setImdbRating(omdbMovie.getImdbRating());
					suggestedMovie.setImdbVotes(omdbMovie.getImdbVotes());
					suggestedMovie.setTomatoMeter(omdbMovie.getTomatoMeter());
					suggestedMovie.setTomatoRating(omdbMovie.getTomatoRating());
					suggestedMovie.setTomatoReviews(omdbMovie.getTomatoReviews());
					suggestedMovie.setTomatoFresh(omdbMovie.getTomatoFresh());
					suggestedMovie.setTomatoRotten(omdbMovie.getTomatoRotten());
					suggestedMovie.setTomatoConsensus(omdbMovie.getTomatoConsensus());
					suggestedMovie.setTomatoUserMeter(omdbMovie.getTomatoUserMeter());
					suggestedMovie.setTomatoUserReviews(omdbMovie.getTomatoUserReviews());
					suggestedMovie.setTomatoUserRating(omdbMovie.getTomatoUserRating());
					suggestedMovie.setTomatoURL(omdbMovie.getTomatoURL());
				}
			}
			}
		}
		//rootData.setUserInfoStats(userInfoStats);
		rootData.setSuggestedMovie(suggestedMovie);

		if (rootData.getSuggestedMovie() == null) {
			if (user == null) {
				userInfoStats.setStatus("fail");
				userInfoStats.setUsername(null);
				userInfoStats.setMessage(
						"No sutable movie found, please change the movie preferences");
			}
			else {
				userInfoStats.setStatus("success");
				userInfoStats.setUsername(user.getUsername());
				userInfoStats.setMessage("No sutable movie found, please change the movie preferences");
			}
		}
		rootData.setUserInfoStats(userInfoStats);
		

		return rootData;
	}

	private List<Movie> clearListFromWatchedMovies(List<WatchedMovie> watchedMovieDb, String genres, String rating, int iteration) {

		if (iteration <= 3) {
			List<Movie> listMovies = tmdbService.listRandomMovies(genres,
					rating);
			if(listMovies != null) {
				for (WatchedMovie movieWatched : watchedMovieDb) {
					for (Movie movieNew : listMovies) {
						if (movieWatched.getMovieName().equals(movieNew.getTitle())) {
							movieNew.setTitle(null);
						}
					}
				}
			}
			else {
				return null;
			}
			
			for(int i=listMovies.size()-1; i>=0; i--) {
				if(listMovies.get(i).getTitle() == null) {
					listMovies.remove(i);
				}
			}

			if (listMovies.isEmpty()) {
				iteration++;
				clearListFromWatchedMovies(watchedMovieDb, genres, rating, iteration);
			}

			return listMovies;
		}

		return null;
	}
	
	public List<String> listOfWatchedMovies(Long userId) {
		List<String> watchedMovies = new ArrayList<String>();
		
		List<WatchedMovie> watchedMoviesRepo = watchedMovieRepository.findAllByUserId(userId);
		
		for (WatchedMovie watchedMovie : watchedMoviesRepo) {
			watchedMovies.add(watchedMovie.getMovieName());
		}
		
		return watchedMovies;
	}

	public UserInfoStats addWatchedMovie(String movieName, String imdbId, Long userId, String username) {

		UserInfoStats userInfoStats = new UserInfoStats();
		Account userById = userRepository.findById(userId);
		Account userByUsername = userRepository.findByUsername(username);
		
		if (userById != null && userById.getId().equals(userByUsername.getId())) {
			WatchedMovie watchedMovie = new WatchedMovie(imdbId, movieName, userById);
			if(imdbId != null) {
				if(watchedMovieRepository.findMovieByImdbIdAndUserId(imdbId, userId) == null) {
					watchedMovieRepository.save(watchedMovie);
					
					userInfoStats.setStatus("success");
					userInfoStats.setUsername(userById.getUsername());
					userInfoStats.setMessage(movieName + " was added to the watched list");
					userInfoStats.setUser_id(userById.getId());
				}
				else {
					userInfoStats.setStatus("failed");
					userInfoStats.setUsername(userById.getUsername());
					userInfoStats.setMessage("Movie is already added to the watched list");
					userInfoStats.setUser_id(userById.getId());
				}
			}
			else {
				userInfoStats.setStatus("failed");
				userInfoStats.setUsername(userById.getUsername());
				userInfoStats.setUser_id(userById.getId());
				userInfoStats.setMessage("Movie not added to the watched list. Inconsistent movie data");
			}
		} else {
			userInfoStats.setStatus("failed");
			userInfoStats.setUsername("null");
			userInfoStats.setMessage("Movie not added to the watched list. Some error occured");
		}

		return userInfoStats;
	}

	public UserInfoStats saveUserPreferences(float rating, String genres,
			Long userId, String username) {

		UserInfoStats userInfoStats = new UserInfoStats();
		Account userById = userRepository.findById(userId);
		Account userByUsername = userRepository.findByUsername(username);
		if (userById != null && userById.getId().equals(userByUsername.getId())) {
			UserPreferences userPreferences = new UserPreferences(rating,
					genres, userById);

			UserPreferences oldPref = userPreferencesRepository
					.findPreferencesByUserId(userById.getId());
			if (oldPref != null) {
				
				oldPref.setUser(userById);
				oldPref.setTmdbRating(rating);
				oldPref.setTmdbGenres(genres);
				userPreferencesRepository.save(oldPref);
			} else {
				userPreferencesRepository.save(userPreferences);
			}
			userInfoStats.setStatus("success");
			userInfoStats.setUsername(userById.getUsername());
			userInfoStats.setMessage("User preferences saved");
			userInfoStats.setUser_id(userById.getId());
		} else {
			userInfoStats.setStatus("fail");
			userInfoStats.setUsername(null);
			userInfoStats.setMessage("User not found");
		}

		return userInfoStats;
	}

	public UserPreferencesResponse loadUserPreferences(Long userId, String username) {
		Account userById = userRepository.findById(userId);
		Account userByUsername = userRepository.findByUsername(username);
		if (userByUsername != null) {
			
			UserPreferences userPreferences = userPreferencesRepository.findPreferencesByUserId(userById
					.getId());

			if(userPreferences != null) {
				UserPreferencesResponse userPrefResponse = new UserPreferencesResponse(
						userPreferences.getUser().getUsername(), 
						userPreferences.getUser().getId(), 
						userPreferences.getTmdbRating(), 
						userPreferences.getRottenTomatoesRating(), 
						userPreferences.getTmdbGenres()
						);
				
				return userPrefResponse;
			}
			else {
				UserPreferencesResponse userPrefResponse = new UserPreferencesResponse(
						userByUsername.getUsername(), 
						userByUsername.getId(), 0, 0, null);
				
				return userPrefResponse;
			}
		}
		return null;
	}
	
	private void fixOMDBMovie(OMDbMovieData movie) {
		if(movie.getTitle() != null) {
			if (movie.getActors().equals("N/A")) {
				movie.setActors(null);
			}
			if (movie.getAwards().equals("N/A")) {
				movie.setAwards(null);
			}
			if (movie.getBoxOffice().equals("N/A")) {
				movie.setBoxOffice(null);
			}
			if (movie.getCountry().equals("N/A")) {
				movie.setCountry(null);
			}
			if (movie.getDirector().equals("N/A")) {
				movie.setDirector(null);
			}
			if (movie.getDvd().equals("N/A")) {
				movie.setDvd(null);
			}
			if (movie.getGenre().equals("N/A")) {
				movie.setGenre(null);
			}
			if (movie.getImdbId().equals("N/A")) {
				movie.setImdbId(null);
			}
			if (movie.getImdbRating().equals("N/A")) {
				movie.setImdbRating(null);
			}
			if (movie.getImdbVotes().equals("N/A")) {
				movie.setImdbVotes(null);
			}
			if (movie.getLanguage().equals("N/A")) {
				movie.setLanguage(null);
			}
			if (movie.getMetascore().equals("N/A")) {
				movie.setMetascore(null);
			}
			if (movie.getPlot().equals("N/A")) {
				movie.setPlot(null);
			}
			if (movie.getPoster().equals("N/A")) {
				movie.setPoster(null);
			}
			if (movie.getProduction().equals("N/A")) {
				movie.setProduction(null);
			}
			if (movie.getRated().equals("N/A")) {
				movie.setRated(null);
			}
			if (movie.getReleased().equals("N/A")) {
				movie.setReleased(null);
			}
			if (movie.getRuntime().equals("N/A")) {
				movie.setRuntime(null);
			}
			if (movie.getTitle().equals("N/A")) {
				movie.setTitle(null);
			}
			if (movie.getTomatoConsensus().equals("N/A")) {
				movie.setTomatoConsensus(null);
			}
			if (movie.getTomatoFresh().equals("N/A")) {
				movie.setTomatoFresh(null);
			}
			if (movie.getTomatoMeter().equals("N/A")) {
				movie.setTomatoMeter(null);
			}
			if (movie.getTomatoRating().equals("N/A")) {
				movie.setTomatoRating(null);
			}
			if (movie.getTomatoReviews().equals("N/A")) {
				movie.setTomatoReviews(null);
			}
			if (movie.getTomatoRotten().equals("N/A")) {
				movie.setTomatoRotten(null);
			}
			if (movie.getTomatoURL().equals("N/A")) {
				movie.setTomatoURL(null);
			}
			if (movie.getTomatoUserMeter().equals("N/A")) {
				movie.setTomatoUserMeter(null);
			}
			if (movie.getTomatoUserRating().equals("N/A")) {
				movie.setTomatoUserRating(null);
			}
			if (movie.getTomatoUserReviews().equals("N/A")) {
				movie.setTomatoUserReviews(null);
			}
			if (movie.getType().equals("N/A")) {
				movie.setType(null);
			}
			if (movie.getWebsite().equals("N/A")) {
				movie.setWebsite(null);
			}
			if (movie.getWriter().equals("N/A")) {
				movie.setWriter(null);
			}
			if (movie.getYear().equals("N/A")) {
				movie.setYear(null);
			}
		}
	}
}
