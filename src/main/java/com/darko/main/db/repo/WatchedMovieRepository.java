package com.darko.main.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.darko.main.db.model.WatchedMovie;

@Repository
public interface WatchedMovieRepository extends JpaRepository<WatchedMovie, Long> {

	WatchedMovie findMovieByImdbIdAndUserId(String imdbId, Long userId);
	
	WatchedMovie findWatchedMoviesForUserById(Long id);
	
	List<WatchedMovie> findAllByUserId(Long id);
	
}
