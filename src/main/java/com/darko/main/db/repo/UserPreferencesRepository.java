package com.darko.main.db.repo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.darko.main.db.model.UserPreferences;

@Repository
public interface UserPreferencesRepository extends CrudRepository<UserPreferences, Long> {
	
	UserPreferences findPreferencesByUserId(Long id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("UPDATE UserPreferences p SET p.tmdbGenres = :tmdbGenres, p.tmdbRating = :tmdbRating WHERE p.id = :userId")
	int updateUserPreferences(
			@Param("userId") Long userId,
			@Param("tmdbGenres") String tmdbGenres,
			@Param("tmdbRating") float tmdbRating);
}
