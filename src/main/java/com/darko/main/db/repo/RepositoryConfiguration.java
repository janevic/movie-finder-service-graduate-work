package com.darko.main.db.repo;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.darko.main.db.model"})
@EnableJpaRepositories(basePackages = {"com.darko.main.db.repo"})
@EnableTransactionManagement
public class RepositoryConfiguration {

}
