package com.darko.main.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.darko.main.db.model.Account;

@Repository
public interface UserRepository extends CrudRepository<Account, Long> {

	Account findByUsername(String username);
	
	Account findById(Long id);
	
	//Account findByCurrentToken(String currentToken);
	
	/*@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Account u set u.currentToken =:currentToken, u.tokenExpirationDateTime =:tokenDateTime where u.id =:userId")
	void updateUsersToken(
			@Param("userId") Long userId, 
			@Param("currentToken") String currentToken,
			@Param("tokenDateTime") DateTime tokenDateTime);*/
	
}
