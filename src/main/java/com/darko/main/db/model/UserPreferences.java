package com.darko.main.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="UserPreferences")
public class UserPreferences {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "tmdbRating")
	private float tmdbRating;
	
	@Column(name = "rottenTomatoesRating")
	private float rottenTomatoesRating;
	
	@Column(name = "tmdbGenres")
	private String tmdbGenres;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Account user;
	
	public UserPreferences() {}

	public UserPreferences(float tmdbRating, String tmdbGenres,
			Account user) {
		super();
		this.tmdbRating = tmdbRating;
		this.tmdbGenres = tmdbGenres;
		this.user = user;
	}

	public UserPreferences(float tmdbRating, float rottenTomatoesRating,
			String tmdbGenres, Account user) {
		super();
		this.tmdbRating = tmdbRating;
		this.rottenTomatoesRating = rottenTomatoesRating;
		this.tmdbGenres = tmdbGenres;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public float getTmdbRating() {
		return tmdbRating;
	}

	public void setTmdbRating(float tmdbRating) {
		this.tmdbRating = tmdbRating;
	}

	public float getRottenTomatoesRating() {
		return rottenTomatoesRating;
	}

	public void setRottenTomatoesRating(float rottenTomatoesRating) {
		this.rottenTomatoesRating = rottenTomatoesRating;
	}

	public String getTmdbGenres() {
		return tmdbGenres;
	}

	public void setTmdbGenres(String tmdbGenres) {
		this.tmdbGenres = tmdbGenres;
	}

	public Account getUser() {
		return user;
	}

	public void setUser(Account user) {
		this.user = user;
	}
	
	
}
