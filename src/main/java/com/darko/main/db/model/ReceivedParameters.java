package com.darko.main.db.model;

public class ReceivedParameters {

	private String genres;
	private float rating;

	public ReceivedParameters() {
		super();
	}

	public ReceivedParameters(String genres, float rating) {
		super();
		this.genres = genres;
		this.rating = rating;
	}
	
	public ReceivedParameters(float rating) {
		super();
		this.rating = rating;
	}

	public ReceivedParameters(String genres) {
		super();
		this.genres = genres;
	}

	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	
	
}
