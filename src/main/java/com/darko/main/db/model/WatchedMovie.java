package com.darko.main.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="WatchedMovies")
public class WatchedMovie {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name = "imdbId", nullable = false)
	private String imdbId;
	
	@Column(name = "movieName")
	private String movieName;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Account user;
	
	public WatchedMovie(){}

	public WatchedMovie(String imdbId, String movieName, Account user) {
		super();
		this.imdbId = imdbId;
		this.movieName = movieName;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public Account getUser() {
		return user;
	}

	public void setUser(Account user) {
		this.user = user;
	}
	
}