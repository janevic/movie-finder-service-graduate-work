package com.darko.main.db.model;

public class ReceivedMovieParameters {

	private String movieName;
	private String imdbId;
	
	public ReceivedMovieParameters() {
		super();
	}

	public ReceivedMovieParameters(String movieName, String imdbId) {
		super();
		this.movieName = movieName;
		this.imdbId = imdbId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}
	
}
