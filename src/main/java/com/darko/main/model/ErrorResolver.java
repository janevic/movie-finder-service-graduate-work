package com.darko.main.model;

public class ErrorResolver {

	private int statusCode;
	private String errorTitle;
	private String userMessage;

	public ErrorResolver(int statusCodeNotFound, String errorTitle,
			String userMessage) {
		super();
		this.statusCode = statusCodeNotFound;
		this.errorTitle = errorTitle;
		this.userMessage = userMessage;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getDevMessage() {
		return errorTitle;
	}

	public void setDevMessage(String errorTitle) {
		this.errorTitle = errorTitle;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}
}
