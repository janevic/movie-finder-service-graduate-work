package com.darko.main.model;

public class UserPreferencesResponse {
	
	private String username;	
	private Long user_id;
	private float tmdbRating;
	private float rottenTomatoesRating;
	private String tmdbGenres;
	
	public UserPreferencesResponse(String username, Long user_id, float tmdbRating,
			float rottenTomatoesRating, String tmdbGenres) {
		super();
		this.username = username;
		this.user_id = user_id;
		this.tmdbRating = tmdbRating;
		this.rottenTomatoesRating = rottenTomatoesRating;
		this.tmdbGenres = tmdbGenres;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public float getTmdbRating() {
		return tmdbRating;
	}

	public void setTmdbRating(float tmdbRating) {
		this.tmdbRating = tmdbRating;
	}

	public float getRottenTomatoesRating() {
		return rottenTomatoesRating;
	}

	public void setRottenTomatoesRating(float rottenTomatoesRating) {
		this.rottenTomatoesRating = rottenTomatoesRating;
	}

	public String getTmdbGenres() {
		return tmdbGenres;
	}

	public void setTmdbGenres(String tmdbGenres) {
		this.tmdbGenres = tmdbGenres;
	}
	
}
