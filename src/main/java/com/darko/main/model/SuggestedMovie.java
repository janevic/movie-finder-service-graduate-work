package com.darko.main.model;

import java.util.List;

import com.darko.TMDB.model.Genre;

public class SuggestedMovie {

	private String imdbId;
	private String theMovieDbId;
	private String title;
	private String description;
	private List<String> youTubeURLTrailers;
	private String youTubeURLMovie;
	private String posterURL;
	private List<Genre> genres;
	private String budget;
	private String homepage;
	private String language;
	private String popularity;
	private String releaseDate;
	private String revenue;
	private String runtime;
	private String voteAverageTMDB;
	private String voteCountTMDB;
	private List<String> director;
	private List<String> writer;
	private List<String> actors;
	private String country;
	private String awards;
	private String metascore;
	private String imdbRating;
	private String imdbVotes;
	private String tomatoMeter;
	private String tomatoRating;
	private String tomatoReviews;
	private String tomatoFresh;
	private String tomatoRotten;
	private String tomatoConsensus;
	private String tomatoUserMeter;
	private String tomatoUserRating;
	private String tomatoUserReviews;
	private String tomatoURL;
	
	//probability coef - float number
	private float probability;

	public SuggestedMovie() {
		super();
	}

	public SuggestedMovie(String imdbId, String title, String description) {
		super();
		this.imdbId = imdbId;
		this.title = title;
		this.description = description;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getTheMovieDbId() {
		return theMovieDbId;
	}

	public void setTheMovieDbId(String theMovieDbId) {
		this.theMovieDbId = theMovieDbId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getYouTubeURLTrailers() {
		return youTubeURLTrailers;
	}

	public void setYouTubeURLTrailers(List<String> youTubeURLTrailers) {
		this.youTubeURLTrailers = youTubeURLTrailers;
	}

	public String getYouTubeURLMovie() {
		return youTubeURLMovie;
	}

	public void setYouTubeURLMovie(String youTubeURLMovie) {
		this.youTubeURLMovie = youTubeURLMovie;
	}

	public String getPosterURL() {
		return posterURL;
	}

	public void setPosterURL(String posterURL) {
		this.posterURL = posterURL;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPopularity() {
		return popularity;
	}

	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRevenue() {
		return revenue;
	}

	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getVoteAverageTMDB() {
		return voteAverageTMDB;
	}

	public void setVoteAverageTMDB(String voteAverageTMDB) {
		this.voteAverageTMDB = voteAverageTMDB;
	}

	public String getVoteCountTMDB() {
		return voteCountTMDB;
	}

	public void setVoteCountTMDB(String voteCountTMDB) {
		this.voteCountTMDB = voteCountTMDB;
	}

	public List<String> getDirector() {
		return director;
	}

	public void setDirector(List<String> director) {
		this.director = director;
	}

	public List<String> getWriter() {
		return writer;
	}

	public void setWriter(List<String> writer) {
		this.writer = writer;
	}

	public List<String> getActors() {
		return actors;
	}

	public void setActors(List<String> actors) {
		this.actors = actors;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public String getAwards() {
		return awards;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMetascore() {
		return metascore;
	}

	public void setMetascore(String metascore) {
		this.metascore = metascore;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}

	public String getTomatoMeter() {
		return tomatoMeter;
	}

	public void setTomatoMeter(String tomatoMeter) {
		this.tomatoMeter = tomatoMeter;
	}

	public String getTomatoRating() {
		return tomatoRating;
	}

	public void setTomatoRating(String tomatoRating) {
		this.tomatoRating = tomatoRating;
	}

	public String getTomatoReviews() {
		return tomatoReviews;
	}

	public void setTomatoReviews(String tomatoReviews) {
		this.tomatoReviews = tomatoReviews;
	}

	public String getTomatoFresh() {
		return tomatoFresh;
	}

	public void setTomatoFresh(String tomatoFresh) {
		this.tomatoFresh = tomatoFresh;
	}

	public String getTomatoRotten() {
		return tomatoRotten;
	}

	public void setTomatoRotten(String tomatoRotten) {
		this.tomatoRotten = tomatoRotten;
	}

	public String getTomatoConsensus() {
		return tomatoConsensus;
	}

	public void setTomatoConsensus(String tomatoConsensus) {
		this.tomatoConsensus = tomatoConsensus;
	}

	public String getTomatoUserMeter() {
		return tomatoUserMeter;
	}

	public void setTomatoUserMeter(String tomatoUserMeter) {
		this.tomatoUserMeter = tomatoUserMeter;
	}

	public String getTomatoUserRating() {
		return tomatoUserRating;
	}

	public void setTomatoUserRating(String tomatoUserRating) {
		this.tomatoUserRating = tomatoUserRating;
	}

	public String getTomatoUserReviews() {
		return tomatoUserReviews;
	}

	public void setTomatoUserReviews(String tomatoUserReviews) {
		this.tomatoUserReviews = tomatoUserReviews;
	}

	public String getTomatoURL() {
		return tomatoURL;
	}

	public void setTomatoURL(String tomatoURL) {
		this.tomatoURL = tomatoURL;
	}

	public float getProbability() {
		return probability;
	}

	public void setProbability(float probability) {
		this.probability = probability;
	}

	@Override
	public String toString() {
		return "SuggestedMovie [imdbId=" + imdbId + ", theMovieDbId="
				+ theMovieDbId + ", title=" + title + ", description="
				+ description + ", youTubeURLTrailer=" + youTubeURLTrailers.size()
				+ ", youTubeURLMovie=" + youTubeURLMovie +", posterURL="
				+ posterURL + ", genres=" + printGenres() + ", budget=" + budget
				+ ", homepage=" + homepage + ", language=" + language
				+ ", popularity=" + popularity + ", releaseDate=" + releaseDate
				+ ", revenue=" + revenue + ", runtime=" + runtime
				+ ", voteAverageTMDB=" + voteAverageTMDB + ", voteCountTMDB="
				+ voteCountTMDB + ", director=" + director + ", writer="
				+ writer + ", actors=" + actors + ", country=" + country
				+ ", awards=" + awards + ", metascore=" + metascore
				+ ", imdbRating=" + imdbRating + ", imdbVotes=" + imdbVotes
				+ ", tomatoMeter=" + tomatoMeter + ", tomatoRating="
				+ tomatoRating + ", tomatoReviews=" + tomatoReviews
				+ ", tomatoFresh=" + tomatoFresh + ", tomatoRotten="
				+ tomatoRotten + ", tomatoConsensus=" + tomatoConsensus
				+ ", tomatoUserMeter=" + tomatoUserMeter
				+ ", tomatoUserReviews=" + tomatoUserReviews + ", tomatoURL="
				+ tomatoURL + "]";
	}
	
	public String printGenres() {
		String str = "";
		for(Genre g : getGenres()) {
			str += " " + g.getName() + ", ";
		}
		return str;
	}
	
}
