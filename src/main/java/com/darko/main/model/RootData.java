package com.darko.main.model;


public class RootData {

	private UserInfoStats userInfoStats;
	
	private SuggestedMovie suggestedMovie;

	public RootData() {
		super();
	}

	public RootData(UserInfoStats userInfoStats, SuggestedMovie omdbMovieData) {
		super();
		this.userInfoStats = userInfoStats;
		this.suggestedMovie = omdbMovieData;
	}

	public UserInfoStats getUserInfoStats() {
		return userInfoStats;
	}

	public void setUserInfoStats(UserInfoStats userInfoStats) {
		this.userInfoStats = userInfoStats;
	}

	public SuggestedMovie getSuggestedMovie() {
		return suggestedMovie;
	}

	public void setSuggestedMovie(SuggestedMovie suggestedMovie) {
		this.suggestedMovie = suggestedMovie;
	}
	
}
