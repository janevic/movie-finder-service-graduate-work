package com.darko.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.darko.main.db.model.Account;
import com.darko.main.model.UserInfoStats;
import com.darko.main.service.AccountService;

@RestController
@RequestMapping("/user")
public class AccountController {
	
	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public UserInfoStats registerUser(@RequestBody(required=true) Account a) {
		
		Account acc = accountService.createAccount(new Account(a.getUsername(), a.getPassword()));
		UserInfoStats infoStats = new UserInfoStats();
		if(acc.getMsg() == null) {
			infoStats.setStatus("success");
			infoStats.setUsername(acc.getUsername());
			infoStats.setUser_id(acc.getId());
			infoStats.setMessage("The user was created");
		}
		else {
			infoStats.setStatus("fail");
			infoStats.setUsername(acc.getUsername());
			infoStats.setUser_id(acc.getId());
			infoStats.setMessage(acc.getMsg());
		}
		return infoStats;
	}

	@RequestMapping(value = "/validity", method = RequestMethod.POST)
	public UserInfoStats loginUser(@AuthenticationPrincipal final UserDetails user) {
		
		Account a = new Account(user.getUsername(), user.getPassword());
		return accountService.checkUserValidity(a);
	}
}
