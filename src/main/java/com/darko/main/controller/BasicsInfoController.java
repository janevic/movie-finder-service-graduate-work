package com.darko.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.darko.TMDB.model.RootGenres;
import com.darko.main.service.BasicInfoService;

@RestController
@RequestMapping("/basicInfo")
public class BasicsInfoController {

	@Autowired
	@Qualifier("BasicsInfoService")
	private BasicInfoService basicInfoService;
	
	@RequestMapping(value = "/genres", method = RequestMethod.GET)
	public RootGenres getGenres() {
		return basicInfoService.genres();
	}
	
}
