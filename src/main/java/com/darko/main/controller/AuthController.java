package com.darko.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.darko.main.db.model.Account;
import com.darko.main.db.model.ReceivedMovieParameters;
import com.darko.main.db.model.ReceivedParameters;
import com.darko.main.db.repo.UserRepository;
import com.darko.main.model.RootData;
import com.darko.main.model.UserInfoStats;
import com.darko.main.model.UserPreferencesResponse;
import com.darko.main.service.IAuthFunctions;
import com.google.api.client.json.Json;

import org.json.JSONObject;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	@Qualifier("AuthService")
	private IAuthFunctions authService;
	
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/movieSuggestion", method = RequestMethod.GET)
	public RootData logedInSuggestion(
			@RequestParam(value = "genres", required = false) String genres,
			@RequestParam(value = "rating", required = false) String rating,
			@AuthenticationPrincipal final UserDetails user) {
		
		Account acc = userRepository.findByUsername(user.getUsername());

		return authService.authMovieSuggestion(genres, rating,
				acc.getId(), acc.getUsername());
	}
	
	@RequestMapping(value = "/watched", method = RequestMethod.PUT)
	public UserInfoStats addToWatchedList(
			@RequestBody(required = true) ReceivedMovieParameters movieParams,
			@AuthenticationPrincipal final UserDetails user) {

		Account acc = userRepository.findByUsername(user.getUsername());
		
		return authService.addWatchedMovie(movieParams.getMovieName(), movieParams.getImdbId(),
				acc.getId(), acc.getUsername());
	}
	
	@RequestMapping(value = "/user/preferences", method = RequestMethod.PUT)
	public UserInfoStats saveUserPreferences(
			@RequestBody(required=true) ReceivedParameters parameters,
			@AuthenticationPrincipal final UserDetails user) {

		Account acc = userRepository.findByUsername(user.getUsername());
		
		return authService.saveUserPreferences(parameters.getRating(), parameters.getGenres(),
				acc.getId(), acc.getUsername());
	}

	@RequestMapping(value = "/user/preferences", method = RequestMethod.GET)
	public UserPreferencesResponse loadUserPreferences(@AuthenticationPrincipal final UserDetails user) {
		
		Account acc = userRepository.findByUsername(user.getUsername());
		
		return authService.loadUserPreferences(acc.getId(),
				acc.getUsername());
	}
	
	@RequestMapping(value = "/watched", method = RequestMethod.GET)
	public List<String> watchedMovies(@AuthenticationPrincipal final UserDetails user) {
		Account acc = userRepository.findByUsername(user.getUsername());
		return authService.listOfWatchedMovies(acc.getId());
	}

}
