package com.darko.main.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.darko.main.model.ErrorResolver;

@RestController
public class CustomErrorController implements ErrorController {

	private static final String PATH = "/error";

    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = PATH)
	public ErrorResolver error(HttpServletResponse response) {
    	String errorTitle = "";
    	String userMessage = "";
    	
    	if(response.getStatus() == 400) {
    		errorTitle = "Bad Request";
    		userMessage = "The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.";
    	}
    	else if(response.getStatus() == 401) {
    		errorTitle = "Unauthorized";
    		userMessage = "The request requires user authentication.";
    	}
    	else if(response.getStatus() == 403) {
    		errorTitle = "Forbidden";
    		userMessage = "The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.";
    	}
    	else if(response.getStatus() == 404) {
    		errorTitle = "Not Found";
    		userMessage = "The server has not found anything matching the Request-URI.";
    	}
    	else if(response.getStatus() == 405) {
    		errorTitle = "Method Not Allowed";
    		userMessage = "The method specified in the Request-Line is not allowed for the resource identified by the Request-URI. The response MUST include an Allow header containing a list of valid methods for the requested resource.";
    	}
    	else if(response.getStatus() == 406) {
    		errorTitle = "Not Acceptable";
    		userMessage = "The resource identified by the request is only capable of generating response entities which have content characteristics not acceptable according to the accept headers sent in the request.";
    	}
    	else if(response.getStatus() == 408) {
    		errorTitle = "Request Timeout";
    		userMessage = "The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time.";
    	}
    	else if(response.getStatus() == 409) {
    		errorTitle = "Conflict";
    		userMessage = "The request could not be completed due to a conflict with the current state of the resource. This code is only allowed in situations where it is expected that the user might be able to resolve the conflict and resubmit the request.";
    	}
    	else if(response.getStatus() == 414) {
    		errorTitle = "Request-URI Too Long";
    		userMessage = "The server is refusing to service the request because the Request-URI is longer than the server is willing to interpret.";
    	}
    	else if(response.getStatus() == 415) {
    		errorTitle = "Unsupported Media Type";
    		userMessage = "The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method.";
    	}
    	else if(response.getStatus() == 500) {
    		errorTitle = "Internal Server Error";
    		userMessage = "The server encountered an unexpected condition which prevented it from fulfilling the request.";
    	}
    	else if(response.getStatus() == 501) {
    		errorTitle = "Not Implemented";
    		userMessage = "The server does not support the functionality required to fulfill the request.";
    	}
    	else if(response.getStatus() == 502) {
    		errorTitle = "Bad Gateway";
    		userMessage = "";
    	}
    	else if(response.getStatus() == 503) {
    		errorTitle = "Service Unavailable";
    		userMessage = "The server is currently unable to handle the request due to a temporary overloading or maintenance of the server. ";
    	}
    	else if(response.getStatus() == 504) {
    		errorTitle = "Gateway Timeout";
    		userMessage = "The server, while acting as a gateway or proxy, did not receive a timely response from the upstream server specified by the URI";
    	}
    	else {
    		errorTitle = "Error";
    		userMessage = "Please contact the administrator.";
    	}
    	
		return new ErrorResolver(response.getStatus(), errorTitle, userMessage);
	}

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return null;
	}
}
