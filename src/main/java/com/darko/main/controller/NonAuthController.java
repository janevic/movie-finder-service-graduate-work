package com.darko.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.darko.main.model.SuggestedMovie;
import com.darko.main.service.INonAuthFunctions;

@RestController
@RequestMapping("/nonauth")
public class NonAuthController {
	
	@Autowired
	@Qualifier("NonAuthService")
	private INonAuthFunctions nonAnthService;
	
	@RequestMapping(value = "/movieSuggestion", method = RequestMethod.GET)
	public SuggestedMovie notLogedInSuggestion(
			@RequestParam(value = "genres", required = false) String genres, 
			@RequestParam(value = "rating", required = false) String rating){
		
		return nonAnthService.movieSuggestion(genres, rating);	
	}
	
	@RequestMapping(value = "/title", method = RequestMethod.GET)
	public List<SuggestedMovie> searchMovieByTitle(@RequestParam(value = "query", required = true) String title) {
		
		return nonAnthService.searchMovieByTitle(title);
	}
	
	@RequestMapping(value = "/movieDetails", method = RequestMethod.GET)
	public SuggestedMovie fullMovieDetails(
			@RequestParam(value = "id", required = true) String id) {
		return nonAnthService.fullMovieDetails(id);
	}
	
}
