package com.darko.YouTube.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.springframework.stereotype.Service;

import com.darko.MovieFinderServiceApplication;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.SearchListResponse;

@Service("WatchMoviesService")
public class WatchMovieService implements IWatchMovies {

	private static final String PROPERTIES_FILENAME = "application.properties";
	private static final long NUMBER_OF_MOVIES = 5;
	private static YouTube youTube;
	
	@Override
	public String findMovieOnYoutube(String movieTitle) {
		
		Properties properties = new Properties();
		
		try {
			InputStream in = MovieFinderServiceApplication.class.getResourceAsStream('/' + PROPERTIES_FILENAME);
			properties.load(in);
		}
		catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		try {
			JsonFactory JSON_FACTORY = new JacksonFactory();
			youTube = new YouTube.Builder(new NetHttpTransport(), JSON_FACTORY, new HttpRequestInitializer() {
				
				@Override
				public void initialize(HttpRequest request) throws IOException {
					
				}
			}).setApplicationName("YouTubeAPITesting").build();
			
			String apiKey = properties.getProperty("apikey");
			
			YouTube.Search.List search = youTube.search().list("snippet");
			search.setKey(apiKey);
			search.setQ(movieTitle);
			search.setMaxResults(NUMBER_OF_MOVIES);
			search.setType("video");
			search.setVideoType("movie");
			
			SearchListResponse searchListResponse = search.execute();
			List<SearchResult> searchResultList = searchListResponse.getItems();
			if(searchResultList != null) {
				if(!searchResultList.iterator().hasNext()) {
					System.out.println("No results");
				}
				
				Iterator<SearchResult> iterator = searchResultList.iterator();
				while(iterator.hasNext()) {
					
					SearchResult video = iterator.next();
					if(video.getSnippet().getTitle().equals(movieTitle)) {
		                return "https://www.youtube.com/embed/"+video.getId().getVideoId();
					}
				}
			}
			
		} catch (IOException e) {
			e.getMessage();
		} catch (Exception t) {
			t.printStackTrace();
		}
		
		return null;
	}

}
