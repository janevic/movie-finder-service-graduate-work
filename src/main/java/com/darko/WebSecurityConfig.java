package com.darko;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.darko.main.service.AccountUserDetailsService;
import com.darko.main.service.EncoderService;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private AccountUserDetailsService accountUserDetailsService;

	@Autowired
	private EncoderService bcryptPasswordEncoder;

	@Override
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		
		authProvider.setPasswordEncoder(bcryptPasswordEncoder.getBCryptEncoder());
		authProvider.setUserDetailsService(accountUserDetailsService);

		auth.authenticationProvider(authProvider);

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.sessionManagement().sessionCreationPolicy(
				SessionCreationPolicy.STATELESS);
		http.csrf().disable();
		http.httpBasic();
		http.authorizeRequests().antMatchers("/auth/**").fullyAuthenticated();
		http.authorizeRequests().antMatchers("/user/register").permitAll();
		http.authorizeRequests().antMatchers("/basicInfo/**").permitAll();
		http.authorizeRequests().antMatchers("/nonauth/**").permitAll();
		http.authorizeRequests().antMatchers("/user/validity").fullyAuthenticated();
	}
}
