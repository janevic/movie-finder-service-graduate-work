package com.darko.OMDB.service;

import com.darko.OMDB.model.OMDbMovieData;

public interface IOMDb {

	OMDbMovieData searchMoviesByIMDbID(String imdbId);
	
	OMDbMovieData searchMoviesByTitle(String title);
}
