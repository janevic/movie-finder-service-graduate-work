package com.darko.OMDB.service;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.darko.OMDB.model.OMDbMovieData;

@Service("OMDbService")
public class OMDbService implements IOMDb {

	@Override
	public OMDbMovieData searchMoviesByIMDbID(String imdbId) {
		// TODO Auto-generated method stub
		
		RestTemplate basicSuggestionRestTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder.fromUriString("http://www.omdbapi.com/?")
				.queryParam("i", imdbId)
				.queryParam("type", "movie")
				.queryParam("plot", "full")
				.queryParam("r", "json")
				.queryParam("tomatoes", true);
		
		URI targetUrlOMDB = uriComponentsBuilder.build().toUri();
		//System.out.println(targetUrlOMDB.toString());
		OMDbMovieData movie = null;
		try {
		movie = basicSuggestionRestTemplate.getForObject(targetUrlOMDB, OMDbMovieData.class);
		} catch(RestClientException error) {
			return null;
			/*e.printStackTrace();
			System.out.println("Service unavailable");*/
		}
		return movie;
	}

	@Override
	public OMDbMovieData searchMoviesByTitle(String title) {
		// TODO Auto-generated method stub
		RestTemplate basicSuggestionRestTemplate = new RestTemplate();
		UriComponentsBuilder uriComponentsBuilder;
		
		uriComponentsBuilder = UriComponentsBuilder.fromUriString("http://www.omdbapi.com/?")
				.queryParam("t", title)
				.queryParam("type", "movie")
				.queryParam("plot", "full")
				.queryParam("r", "json")
				.queryParam("tomatoes", true);
		
		URI targetUrlOMDB = uriComponentsBuilder.build().toUri();
		//System.out.println(targetUrlOMDB.toString());
		OMDbMovieData movie = null;
		try {
		movie = basicSuggestionRestTemplate.getForObject(targetUrlOMDB, OMDbMovieData.class);
		} catch(RestClientException error) {
			return null;
		}
		//OMDbMovieData movie = basicSuggestionRestTemplate.getForObject(targetUrlOMDB, OMDbMovieData.class);
		
		return movie;
	}

}
