package com.darko.OMDB.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OMDbMovieData {

	//http://www.omdbapi.com/?t=Interstellar&y=&plot=full&tomatoes=true&type=movie&r=json
	
	@JsonProperty("Title")
	private String title;
	
	@JsonProperty("Year")
	private String year;
	
	@JsonProperty("Rated")
	private String rated;
	
	@JsonProperty("Released")
	private String released;
	
	@JsonProperty("Runtime")
	private String runtime;
	
	@JsonProperty("Genre")
	private String genre;
	
	//private List<Genre> genres;
	
	@JsonProperty("Director")
	private String director;
	
	@JsonProperty("Writer")
	private String writer;
	
	@JsonProperty("Actors")
	private String actors;
	
	@JsonProperty("Plot")
	private String plot;
	
	@JsonProperty("Language")
	private String language;
	
	@JsonProperty("Country")
	private String country;
	
	@JsonProperty("Awards")
	private String awards;
	
	@JsonProperty("Poster")
	private String poster;
	
	@JsonProperty("Metascore")
	private String metascore;
	
	@JsonProperty("imdbRating")
	private String imdbRating;
	
	@JsonProperty("imdbVotes")
	private String imdbVotes;
	
	@JsonProperty("imdbID")
	private String imdbId;
	
	@JsonProperty("Type")
	private String type;
	
	@JsonProperty("tomatoMeter")
	private String tomatoMeter;
	
	@JsonProperty("tomatoRating")
	private String tomatoRating;
	
	@JsonProperty("tomatoReviews")
	private String tomatoReviews;
	
	@JsonProperty("tomatoFresh")
	private String tomatoFresh;
	
	@JsonProperty("tomatoRotten")
	private String tomatoRotten;
	
	@JsonProperty("tomatoConsensus")
	private String tomatoConsensus;
	
	@JsonProperty("tomatoUserMeter")
	private String tomatoUserMeter;
	
	@JsonProperty("tomatoUserReviews")
	private String tomatoUserReviews;
	
	@JsonProperty("tomatoUserRating")
	private String tomatoUserRating;
	
	@JsonProperty("tomatoURL")
	private String tomatoURL;
	
	@JsonProperty("DVD")
	private String dvd;
	
	@JsonProperty("BoxOffice")
	private String boxOffice;
	
	@JsonProperty("Production")
	private String production;
	
	@JsonProperty("Website")
	private String website;

	public OMDbMovieData() {
		super();
	}

	public OMDbMovieData(String title, String year, String genre, String plot,
			String imdbId) {
		super();
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.plot = plot;
		this.imdbId = imdbId;
	}

	/*public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}*/

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getRated() {
		return rated;
	}

	public void setRated(String rated) {
		this.rated = rated;
	}

	public String getReleased() {
		return released;
	}

	public void setReleased(String released) {
		this.released = released;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getMetascore() {
		return metascore;
	}

	public void setMetascore(String metascore) {
		this.metascore = metascore;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTomatoMeter() {
		return tomatoMeter;
	}

	public void setTomatoMeter(String tomatoMeter) {
		this.tomatoMeter = tomatoMeter;
	}

	public String getTomatoRating() {
		return tomatoRating;
	}

	public void setTomatoRating(String tomatoRating) {
		this.tomatoRating = tomatoRating;
	}

	public String getTomatoReviews() {
		return tomatoReviews;
	}

	public void setTomatoReviews(String tomatoReviews) {
		this.tomatoReviews = tomatoReviews;
	}

	public String getTomatoFresh() {
		return tomatoFresh;
	}

	public void setTomatoFresh(String tomatoFresh) {
		this.tomatoFresh = tomatoFresh;
	}

	public String getTomatoRotten() {
		return tomatoRotten;
	}

	public void setTomatoRotten(String tomatoRotten) {
		this.tomatoRotten = tomatoRotten;
	}

	public String getTomatoConsensus() {
		return tomatoConsensus;
	}

	public void setTomatoConsensus(String tomatoConsensus) {
		this.tomatoConsensus = tomatoConsensus;
	}

	public String getTomatoUserMeter() {
		return tomatoUserMeter;
	}

	public void setTomatoUserMeter(String tomatoUserMeter) {
		this.tomatoUserMeter = tomatoUserMeter;
	}

	public String getTomatoUserRating() {
		return tomatoUserRating;
	}

	public void setTomatoUserRating(String tomatoUserRating) {
		this.tomatoUserRating = tomatoUserRating;
	}

	public String getTomatoUserReviews() {
		return tomatoUserReviews;
	}

	public void setTomatoUserReviews(String tomatoUserReviews) {
		this.tomatoUserReviews = tomatoUserReviews;
	}

	public String getTomatoURL() {
		return tomatoURL;
	}

	public void setTomatoURL(String tomatoURL) {
		this.tomatoURL = tomatoURL;
	}

	public String getDvd() {
		return dvd;
	}

	public void setDvd(String dvd) {
		this.dvd = dvd;
	}

	public String getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(String boxOffice) {
		this.boxOffice = boxOffice;
	}

	public String getProduction() {
		return production;
	}

	public void setProduction(String production) {
		this.production = production;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
	public String toString() {
		return "OMDbMovieData [title=" + title + ", year=" + year + ", rated="
				+ rated + ", released=" + released + ", runtime=" + runtime
				+ ", genre=" + genre + ", director=" + director + ", writer="
				+ writer + ", actors=" + actors + ", plot=" + plot
				+ ", language=" + language + ", country=" + country
				+ ", awards=" + awards + ", poster=" + poster + ", metascore="
				+ metascore + ", imdbRating=" + imdbRating + ", imdbVotes="
				+ imdbVotes + ", imdbId=" + imdbId + ", type=" + type
				+ ", tomatoMeter=" + tomatoMeter + ", tomatoRating="
				+ tomatoRating + ", tomatoReviews=" + tomatoReviews
				+ ", tomatoFresh=" + tomatoFresh + ", tomatoRotten="
				+ tomatoRotten + ", tomatoConsensus=" + tomatoConsensus
				+ ", tomatoUserMeter=" + tomatoUserMeter
				+ ", tomatoUserReviews=" + tomatoUserReviews + ", tomatoURL="
				+ tomatoURL + ", dvd=" + dvd + ", boxOffice=" + boxOffice
				+ ", production=" + production + ", website=" + website + "]";
	}
	
	/*public String printGenres() {
		String str = "";
		for(Genre g : getGenres()) {
			str += " " + g.getName() + ", ";
		}
		return str;
	}*/
	
}
